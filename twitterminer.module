<?php

/**
 * @file
 * Mines twitter for data, based on hashtag, user, or search term.
 */


define('TWITTERMINER_SEARCH_URL', 'http://search.twitter.com/search.json?q=');


/*************************************************************
 * Hooks
 *************************************************************/






/**
 * Implementation of hook_perm().
 */
function twitterminer_perm() {
  return array('configure twitter mining');
}






/**
 * Implementation of hook_menu().
 */
function twitterminer_menu() {

  $items = array();

  // Settings page.
  $items['admin/settings/twitterminer'] = array(
    'title' =>  'TwitterMiner Settings',
    'description'   => 'Configure the twitter search terms.',
    'page callback' => 'twitterminer_admin_settings_page',
    'access callback' => 'user_access',
    'access arguments' => array('configure twitter mining'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'twitterminer.admin.inc',
  );

  // Settings page, search form, create.
  $items['admin/settings/twitterminer/search'] = array(
    'title' =>  'Configure TwitterMiner Search',
    'description'   => 'Add or edit a TwitterMiner search.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('twitterminer_search_form'),
    'access callback' => 'user_access',
    'access arguments' => array('configure twitter mining'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'twitterminer.admin.inc',
  );

  // Settings page, search form, delete.
  $items['admin/settings/twitterminer/search/%/delete'] = array(
    'title' =>  'Configure TwitterMiner Search',
    'description'   => 'Delete TwitterMiner Search.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('twitterminer_delete_search_form', 4),
    'access callback' => 'user_access',
    'access arguments' => array('configure twitter mining'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'twitterminer.admin.inc',
  );
  return $items;
}





/**
 * Implementation of hook_cron().
 */
function twitterminer_cron() {

  $symbols = array(
    'hashtag' => '%23',
    'from_user' => 'from%3A',
    'to_user' => 'to%3A',
    'reference_user' => '%40',
    'text' => '',
  );

  $sql = "SELECT * FROM {twitterminer_search}";
  $searches = db_query($sql);

  while ($search = db_fetch_object($searches)) { 

    $sql = "SELECT MAX(id_str) FROM {twitterminer_tweet} WHERE tmsid = %d";
    $max_twitter_id_str = db_result(db_query($sql, $search->tmsid));

    $q = $symbols[$search->tmtype] . $search->tmterm;
    $lang = (empty($search->tmlanguage)) ? '' : '&lang=' . $search->tmlanguage;
    $rpp = '&rpp=100';
    $since_id = '&since_id=' . $max_twitter_id_str;

    $url = TWITTERMINER_SEARCH_URL . $q . $lang . $rpp . $since_id;

    twitterminer_save_tweets($url, $search);
  }
}





/*************************************************************
 * Custom functions
 *************************************************************/





/**
 * Grabs tweets from twitter and saves them in the database.
 */
function twitterminer_save_tweets($url, $search) {

  $tweets = twitterminer_get_tweets($url);

  // Loop through tweets and store each one in the database.
  if (!empty($tweets['results'])) {
    foreach ($tweets['results'] as $tweet) {
      twitterminer_save_tweet($tweet, $search->tmsid);
    }
  }
}



/**
 * Grabs json file via curl and turn it into a php object.
 */
function twitterminer_get_tweets($url) {
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // This crashes cron
  curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);
  $out = curl_exec($ch);                                                                                                                                      
  curl_close($ch);
  $out = json_decode($out, TRUE);

  return $out;
}





function twitterminer_save_tweet($tweet, $tmsid) {

  $sql = "INSERT INTO {twitterminer_tweet} ";
  $sql .= "(tmsid, from_user_id_str, profile_image_url, created_at, from_user, id_str, metadata, to_user_id, text, id, from_user_id, geo, iso_language_code, to_user_id_str, source) ";
  $sql .= "VALUES ('%s', '%s',  '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";

  db_query($sql,
    $tmsid,
    $tweet['from_user_id_str'],
    $tweet['profile_image_url'],
    $tweet['created_at'],
    $tweet['from_user'],
    $tweet['id_str'],
    serialize($tweet['metadata']),
    $tweet['to_user_id'],
    $tweet['text'],
    $tweet['id'],
    $tweet['from_user_id'],
    serialize($tweet['geo']),
    $tweet['iso_language_code'],
    $tweet['to_user_id_str'],
    $tweet['source']);
}





/*************************************************************
 * Debugging assistance
 *************************************************************/





/**
 * Debugging output made pretty. Object-aware.
 */
function twitterminer_ds($v) {
  if (is_object($v)) {
    drupal_set_message('<pre>' . print_r((array) $v, 1) . '</pre>');
  }

  else {
    drupal_set_message('<pre>' . print_r($v, 1) . '</pre>');
  }
}




/**
 *  More help with debugging.
 */
function twitterminer_called() {
  $f = debug_backtrace();
  twitterminer_ds($f[2]['function'] . '->' . $f[1]['function']);
}
