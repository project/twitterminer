<?php

/**
 * @file
 * Administrative page callbacks for the twitterminer module.
 */





/**
 * Callback for admin settings page (admin/settings/twitterminer).
 *
 * @return
 *    Themed HTML table of all the Twitter searches stored in the Drupal database.
 */
function twitterminer_admin_settings_page() {

  $add_search_link = l('Add a TwitterMiner search', 'admin/settings/twitterminer/search');
  drupal_set_message($add_search_link);

  $head = array('tmsid', 'Type', 'Text', 'Language', 'Actions');
  $rows = array();

  // Get connections from database.
  $query = db_query("SELECT * FROM {twitterminer_search} ORDER BY tmsid");
  $i = 0;

  // Loop through connections and add to each one to the $rows array.
  while ($row = db_fetch_array($query)) {
    $rows[$i] = $row;
    $edit_link = l('Edit', 'admin/settings/twitterminer/search/' . $rows[$i]['tmsid']);
    $delete_link = l('Delete', 'admin/settings/twitterminer/search/' . $rows[$i]['tmsid'] . '/delete');
    $rows[$i]['actions'] = $edit_link . ' | ' . $delete_link;
    $i++;
  }

  return theme_table($head, $rows);
}





/**
 * Form to add or edit a single FileMaker connection.
 */
function twitterminer_search_form($form_state, $tmsid = NULL) {

  // Record being inserted or updated?
  if ($tmsid) {
    $sql = "SELECT * FROM {twitterminer_search} WHERE tmsid = %d";
    $search = db_fetch_object(db_query($sql, $tmsid));
  }

  $form = array();

  // Insert validation, submit function names, and redirect location.
  $form['#submit'][] = 'twitterminer_search_submit';
  $form['#redirect'] = 'admin/settings/twitterminer';

  // Create search fieldset.
  $form['search'] = array(
    '#title' => t('TwitterMiner Search'),
    '#type' => 'fieldset',
  );

  // Create search fieldset.
  $form['search']['tmsid'] = array(
    '#type' => 'hidden',
    '#value' => $tmsid,
  );

  // Type of search field.
  $options = array(
    'hashtag' => 'Hashtag',
    'to_user' => 'To user',
    'from_user' => 'From user',
    'reference_user' => 'Refrences user',
    'text' => 'Text'
  );
  $form['search']['tmtype'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $search->tmtype,
    '#required' => TRUE,
  );

  // Text (search criteria) field.
  $form['search']['tmterm'] = array(
    '#title' => t('Term (exclude the \'@\' or \'#\')'),
    '#type' => 'textfield',
    '#default_value' => $search->tmterm,
    '#required' => TRUE,
  );

  // Language field.
  $options = twitterminer_languages();
  $form['search']['tmlanguage'] = array(
    '#title' => t('Language'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $search->tmlanguage,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save TwitterMiner Search'),
  );

  return $form;
}





/**
 * Submit handler for twitterminer_connection_form.
 */
function twitterminer_search_submit(&$form, &$form_state) {

  $tmsid = $form_state['values']['tmsid'];
  $tmtype = $form_state['values']['tmtype'];
  $tmterm = $form_state['values']['tmterm'];
  $tmlanguage = $form_state['values']['tmlanguage'];

  // Update connection?
  if ($tmsid) {
    $sql = "UPDATE {twitterminer_search} SET tmtype = '%s', tmterm = '%s', tmlanguage = '%s' WHERE tmsid = %d";
    db_query($sql, $tmtype, $tmterm, $tmlanguage, $tmsid);
    drupal_set_message(t('TwitterMiner search updated.'));
    watchdog('twitterminer', 'TwitterMiner search updated', array(), WATCHDOG_INFO);
  }

  // Insert connection.
  else {
    $sql = "INSERT INTO {twitterminer_search} (tmtype, tmterm, tmlanguage) VALUES ('%s', '%s', '%s')";
    db_query($sql, $tmtype, $tmterm, $tmlanguage);
    drupal_set_message(t('TwitterMiner search created.'));
    watchdog('twitterminer', 'TwitterMiner search created', array(), WATCHDOG_INFO);
  }
}





/**
 * Callback for the filemaker_delete_form.
 */
function twitterminer_delete_search_form($form_state, $tmsid) {

  $form = array();
  $form['#submit'][] = 'twitterminer_delete_search_form_submit';
  $form['#redirect'][] = 'admin/settings/twitterminer';

  // Create TwitterMiner fieldset.
  $form['twitterminer'] = array(
    '#title' => t('Confirm Deletion'),
    '#type' => 'fieldset',
  );

  // Store id. Could be fmid, fmfid, or fmpid.
  $form['twitterminer']['tmsid'] = array(
    '#type' => 'hidden',
    '#value' => $tmsid,
  );

  // Markup.
  $form['twitterminer']['markup'] = array(
    '#type' => 'markup',
    '#title' => t('Confirm Deletion'),
    '#value' => t('Are you sure you want to delete this TwitterMiner search (mined data will not be deleted)?'),
  );

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete TwitterMiner Search'),
    '#weight' => 50,
  );

  return $form;
}






/**
 * Deletes a single TwitterMiner search.
 */
function twitterminer_delete_search_form_submit($form, $form_state) {

  $sql = "DELETE FROM {twitterminer_search} WHERE tmsid = %d";
  $result = db_query($sql, $form_state['values']['tmsid']);

  drupal_set_message(t('TwitterMiner search deleted.'));
  watchdog('filemaker', 'TwitterMiner search deleted', array(), WATCHDOG_INFO);
}






/**
 * Returns an array of languages using the ISO-639-1 code.
 */
function twitterminer_languages() {

  $options = array(
    '' => t('All languages'),
    'aa' => t('Afar'),
    'ab' => t('Abkhazian'),
    'af' => t('Afrikaans'),
    'ak' => t('Akan'),
    'am' => t('Amharic'),
    'ar' => t('Arabic'),
    'an' => t('Aragonese'),
    'hy' => t('Armenian'),
    'as' => t('Assamese'),
    'av' => t('Avaric'),
    'ae' => t('Avestan'),
    'ay' => t('Aymara'),
    'az' => t('Azerbaijani'),
    'ba' => t('Bashkir'),
    'bm' => t('Bambara'),
    'eu' => t('Basque'),
    'be' => t('Belarusian'),
    'bn' => t('Bengali'),
    'bh' => t('Bihari'),
    'bi' => t('Bislama'),
    'bo' => t('Tibetan'),
    'bs' => t('Bosnian'),
    'br' => t('Breton'),
    'bg' => t('Bulgarian'),
    'ca' => t('Catalan; Valencian'),
    'ch' => t('Chamorro'),
    'ce' => t('Chechen'),
    'zh' => t('Chinese'),
    'cu' => t('Church Slavic; Old Slavonic'),
    'cv' => t('Chuvash'),
    'kw' => t('Cornish'),
    'co' => t('Corsican'),
    'cr' => t('Cree'),
    'cy' => t('Welsh'),
    'cs' => t('Czech'),
    'da' => t('Danish'),
    'dv' => t('Divehi; Dhivehi; Maldivian'),
    'dz' => t('Dzongkha'),
    'en' => t('English'),
    'eo' => t('Esperanto'),
    'et' => t('Estonian'),
    'ee' => t('Ewe'),
    'fo' => t('Faroese'),
    'fj' => t('Fijian'),
    'fi' => t('Finnish'),
    'fr' => t('French'),
    'fy' => t('Western Frisian'),
    'ff' => t('Fulah'),
    'ka' => t('Georgian'),
    'de' => t('German'),
    'gd' => t('Gaelic; Scottish Gaelic'),
    'ga' => t('Irish'),
    'gl' => t('Galician'),
    'gv' => t('Manx'),
    'el' => t('Greek, Modern (1453-)'),
    'gn' => t('Guarani'),
    'gu' => t('Gujarati'),
    'ht' => t('Haitian; Haitian Creole'),
    'ha' => t('Hausa'),
    'he' => t('Hebrew'),
    'hz' => t('Herero'),
    'hi' => t('Hindi'),
    'ho' => t('Hiri Motu'),
    'hr' => t('Croatian'),
    'hu' => t('Hungarian'),
    'ig' => t('Igbo'),
    'is' => t('Icelandic'),
    'io' => t('Ido'),
    'ii' => t('Sichuan Yi; Nuosu'),
    'iu' => t('Inuktitut'),
    'ie' => t('Interlingue; Occidental'),
    'ia' => t('Interlingua (International Auxiliary Language Association)'),
    'id' => t('Indonesian'),
    'ik' => t('Inupiaq'),
    'it' => t('Italian'),
    'jv' => t('Javanese'),
    'ja' => t('Japanese'),
    'kl' => t('Kalaallisut; Greenlandic'),
    'kn' => t('Kannada'),
    'ks' => t('Kashmiri'),
    'kr' => t('Kanuri'),
    'kk' => t('Kazakh'),
    'km' => t('Central Khmer'),
    'ki' => t('Kikuyu; Gikuyu'),
    'rw' => t('Kinyarwanda'),
    'ky' => t('Kirghiz; Kyrgyz'),
    'kv' => t('Komi'),
    'kg' => t('Kongo'),
    'ko' => t('Korean'),
    'kj' => t('Kuanyama; Kwanyama'),
    'ku' => t('Kurdish'),
    'lo' => t('Lao'),
    'la' => t('Latin'),
    'lv' => t('Latvian'),
    'li' => t('Limburgan; Limburger; Limburgish'),
    'ln' => t('Lingala'),
    'lt' => t('Lithuanian'),
    'lb' => t('Luxembourgish; Letzeburgesch'),
    'lu' => t('Luba-Katanga'),
    'lg' => t('Ganda'),
    'mk' => t('Macedonian'),
    'mh' => t('Marshallese'),
    'ml' => t('Malayalam'),
    'mi' => t('Maori'),
    'mr' => t('Marathi'),
    'mg' => t('Malagasy'),
    'mt' => t('Maltese'),
    'mn' => t('Mongolian'),
    'ms' => t('Malay'),
    'my' => t('Burmese'),
    'na' => t('Nauru'),
    'nv' => t('Navajo; Navaho'),
    'nr' => t('Ndebele, South; South Ndebele'),
    'nd' => t('Ndebele, North; North Ndebele'),
    'ng' => t('Ndonga'),
    'ne' => t('Nepali'),
    'nl' => t('Dutch; Flemish'),
    'nn' => t('Norwegian Nynorsk; Nynorsk, Norwegian'),
    'nb' => t('BokmÃ¥l, Norwegian; Norwegian BokmÃ¥l'),
    'no' => t('Norwegian'),
    'ny' => t('Chichewa; Chewa; Nyanja'),
    'oc' => t('Occitan (post 1500)'),
    'oj' => t('Ojibwa'),
    'or' => t('Oriya'),
    'om' => t('Oromo'),
    'os' => t('Ossetian; Ossetic'),
    'pa' => t('Panjabi; Punjabi'),
    'fa' => t('Persian'),
    'pi' => t('Pali'),
    'pl' => t('Polish'),
    'pt' => t('Portuguese'),
    'ps' => t('Pushto; Pashto'),
    'qu' => t('Quechua'),
    'rm' => t('Romansh'),
    'ro' => t('Romanian; Moldavian; Moldovan'),
    'rn' => t('Rundi'),
    'ru' => t('Russian'),
    'sg' => t('Sango'),
    'sa' => t('Sanskrit'),
    'si' => t('Sinhala; Sinhalese'),
    'sk' => t('Slovak'),
    'sl' => t('Slovenian'),
    'se' => t('Northern Sami'),
    'sm' => t('Samoan'),
    'sn' => t('Shona'),
    'sd' => t('Sindhi'),
    'so' => t('Somali'),
    'st' => t('Sotho, Southern'),
    'es' => t('Spanish; Castilian'),
    'sq' => t('Albanian'),
    'sc' => t('Sardinian'),
    'sr' => t('Serbian'),
    'ss' => t('Swati'),
    'su' => t('Sundanese'),
    'sw' => t('Swahili'),
    'sv' => t('Swedish'),
    'ty' => t('Tahitian'),
    'ta' => t('Tamil'),
    'tt' => t('Tatar'),
    'te' => t('Telugu'),
    'tg' => t('Tajik'),
    'tl' => t('Tagalog'),
    'th' => t('Thai'),
    'ti' => t('Tigrinya'),
    'to' => t('Tonga (Tonga Islands)'),
    'tn' => t('Tswana'),
    'ts' => t('Tsonga'),
    'tk' => t('Turkmen'),
    'tr' => t('Turkish'),
    'tw' => t('Twi'),
    'ug' => t('Uighur; Uyghur'),
    'uk' => t('Ukrainian'),
    'ur' => t('Urdu'),
    'uz' => t('Uzbek'),
    've' => t('Venda'),
    'vi' => t('Vietnamese'),
    'vo' => t('VolapÃ¼k'),
    'wa' => t('Walloon'),
    'wo' => t('Wolof'),
    'xh' => t('Xhosa'),
    'yi' => t('Yiddish'),
    'yo' => t('Yoruba'),
    'za' => t('Zhuang; Chuang'),
    'zu' => t('Zulu'),
  );

  return $options;
}
